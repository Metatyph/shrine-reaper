﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraController : MonoBehaviour
{
    public Transform target;

    public float smoothSpeed;
    public float rtSmoothSpeed;
    public float smoothDashSpeed;
    public Vector3 offset;

    void Start()
    {
        rtSmoothSpeed = smoothSpeed;
    }

    void LateUpdate()
    {
        Vector3 desiredPosition = target.position + offset;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, rtSmoothSpeed);
        transform.position = smoothedPosition;
    }

    public void CameraShake(string dir, float intensity)
    {
        // Shake camera to direction, by intensity
        if (dir == "x")
        {
            transform.position += new Vector3(intensity, 0, 0);
        } else if (dir == "y") {
            transform.position += new Vector3(0, intensity, 0);
        } else {
            // Obligatory error message
            Debug.LogError("Invalid CameraShake direction.");
        }
    }
}
