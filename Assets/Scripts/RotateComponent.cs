﻿using UnityEngine;

public class RotateComponent : MonoBehaviour
{
    public float speed;
    
    void Update()
    {
        transform.Rotate(new Vector3(0, 0, speed * Time.deltaTime));
    }
}
