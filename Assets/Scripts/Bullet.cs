﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed;
    public float lifeTime;
    public float damage;

    void Start()
    {
        //Invoke("DestroySelf", lifeTime);
    }

    void Update()
    {
        transform.Translate(Vector2.up * speed * Time.deltaTime);
    }

    void OnBecameInvisible()
    {
        DestroySelf();
    }

    void DestroySelf()
    {
        Destroy(gameObject);
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Enemy")
        {
            // Get enemy script
            Enemy enm = col.gameObject.GetComponent<Enemy>();

            // Get player script
            Player ply = GameObject.Find("Player").GetComponent<Player>();

            // Modify enemy values on hit
            enm.health -= damage;
            enm.DmgFeedback();
            enm.BossDmg();

            // Modify player values on hit
            if (ply.dashAmt <= ply.dashMax)
            {
                if (ply.dashAmt + ply.dashInc < ply.dashMax)
                    ply.dashAmt += ply.dashInc;
                else
                    ply.dashAmt += ply.dashMax - ply.dashAmt;

                ply.UpdateDashUI();
            }

            // Destroy enemy if 0 health
            if (enm.health <= 0)
                enm.DestroySelf();

            // Destroy self 
            DestroySelf();
        }
    }
}
