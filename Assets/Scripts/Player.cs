﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
	[Header("UI Elements")]
    public GameObject dashBarFill;
    public GameObject healthBarFill;

    [Header("Health")]
    public int healthMax;
    int health;
    bool isDown = false;
    ParticleSystem ps;

    [Header("Movement")]
    public float moveSpd;
    Rigidbody2D rb2d;
    float rtMoveSpd;
    public float movHor;
    public float movVert;

    [Header("Sprites")]
    SpriteRenderer sr;
    Animator anim;
    bool isMoving = false;
    public Sprite idle;
    public Sprite dashOn;
    public Sprite dashActive;
    public Sprite down;

    [Header("Targetting & Projectiles")]
    public GameObject targetting;
    public GameObject bulletPrefab;

    [Header("Bullets")]
    public float bulletSpeed;
    public float bulletLife;
    public float bulletDmg;
    float rtBulletSpeed;
    float rtBulletLife;
    float rtBulletDmg;

    [Header("Dashing")]
    public float dashDmg;
    public float dashMax;
    public float dashAmt;
    public float dashInc;
    float rtDashDmg;
    LineRenderer lr;
    bool canDash = false;

    [Header("SFX")]
    public AudioClip swordFx;
    public AudioClip gunFx;
    public AudioClip dashOnFx;
    public AudioClip dashOn2Fx;
    public AudioClip hurtFx;
    public AudioClip downFx;
    AudioSource audioSource;

    [Header("Post Processing")]
    public PostProcessProfile ppProf;
    CameraController cam;

	void Start()
    {
        // Initialize components
        rb2d = GetComponent<Rigidbody2D>();
        ps = GetComponent<ParticleSystem>();
        audioSource = GetComponent<AudioSource>();
        lr = GetComponent<LineRenderer>();
        sr = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();

        // Initialize all runtime values
        rtMoveSpd = moveSpd;
        rtBulletSpeed = bulletSpeed;
        rtBulletLife = bulletLife;
        rtBulletDmg = bulletDmg;
        rtDashDmg = dashDmg;
        health = healthMax;

		// Set dispensable values to max
		dashAmt = dashMax;

        // Initialize UI
        cam = Camera.main.GetComponent<CameraController>();
        UpdateDashUI();

		// Initialize post processing stack
		ppProf.GetSetting<ColorGrading>().saturation.Override(0f);

	}

	void Update()
	{
        if(Time.timeScale >= 0.045)
        {
            PlayerMovement();
            PlayerRotation();
            PlayerAttack();
            PlayerDash();
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        {
            if (col.gameObject.tag == "EnemyProj" && !canDash && !isDown)
            {
                audioSource.PlayOneShot(hurtFx, .25f);
                health -= 1;
                UpdateHealthUI();
                Destroy(col.gameObject);

                if (health == 0)
                {
                    isDown = true;
                    ps.Play();
                    audioSource.PlayOneShot(downFx, 1f);

                    // Set animation
                    anim.SetBool("isKO", true);
                }
            }
        }
    }

    public void UpdateDashUI()
    {
        dashBarFill.transform.localScale = new Vector3(dashAmt / dashMax, 1, 1);
    }

    public void UpdateHealthUI()
    {
        healthBarFill.transform.localScale = new Vector3((float)health / healthMax, 1, 1);
    }

    void PlayerRotation()
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos = Camera.main.ScreenToWorldPoint(mousePos);

        Vector2 direction = new Vector2(mousePos.x - targetting.transform.position.x, mousePos.y - targetting.transform.position.y);
        targetting.transform.up = direction;

        if(targetting.transform.rotation.z < Quaternion.Euler(0, 0, 0).z && targetting.transform.rotation.z > Quaternion.Euler(0, 0, -180).z)
            sr.flipX = true;
        else
            sr.flipX = false;
    }

    void PlayerMovement()
    {
        if (!canDash && !isDown)
        {
            // Horizontal movement
            if (Input.GetButton("Horizontal"))
            {
                movHor = rtMoveSpd * Input.GetAxisRaw("Horizontal");
            }
            else
            {
                movHor = 0;
            }

            // Vertical movement
            if (Input.GetButton("Vertical"))
            {
                movVert = rtMoveSpd * Input.GetAxisRaw("Vertical");
            }
            else
            {
                movVert = 0;
            }

            // If moving diagonally, reduce movement speed
            if (Input.GetButton("Horizontal") && Input.GetButton("Vertical"))
            {
                rtMoveSpd = moveSpd * .65f;
            }

            // Animate if moving
            if(movHor != 0 || movVert != 0)
            {
                anim.SetBool("isMoving", true);
            } else
            {
                anim.SetBool("isMoving", false);
            }

			// Move player
			transform.Translate(movHor, movVert, 0);

        }
    }

    void PlayerAttack()
    {
        // If not downed, fire a bullet
        if (Input.GetButtonDown("Fire1") && !canDash && !isDown)
        {
            // Instantiate bullets as bulletClone
            GameObject bulletClone;
            bulletClone = Instantiate(bulletPrefab, transform.position, targetting.transform.rotation);

            // Get bullet script from instantiated bullets
            Bullet bul = bulletClone.gameObject.GetComponent<Bullet>();

            // Modify bullet variables
            bul.speed = rtBulletSpeed;
            bul.lifeTime = rtBulletLife;
            bul.damage = rtBulletDmg;

            // Camera shake
            if ((targetting.transform.rotation.z < Quaternion.Euler(0, 0, -45).z && targetting.transform.rotation.z > Quaternion.Euler(0, 0, -135).z) || (targetting.transform.rotation.z > Quaternion.Euler(0, 0, 45).z && targetting.transform.rotation.z < Quaternion.Euler(0, 0, 135).z)) 
                cam.CameraShake("x", -.1f * Mathf.Sign(targetting.transform.rotation.z));
            else
            {
                if(targetting.transform.rotation.z < Quaternion.Euler(0, 0, 45).z && targetting.transform.rotation.z > Quaternion.Euler(0, 0, -45).z)
                    cam.CameraShake("y", .1f);
                else
                    cam.CameraShake("y", -.1f);
            }

            // SFX
            audioSource.PlayOneShot(gunFx, .25f);
        }
        else if (Input.GetButtonDown("Fire1") && isDown) // If downed, attempt revive
        {
            if (health < healthMax)
            {
                health += 1;
                audioSource.PlayOneShot(dashOnFx, (float)health / healthMax);
                UpdateHealthUI();
                cam.CameraShake("x", .01f * health);
            }
            else if (health >= healthMax)
            {
                ps.Play();
                health = healthMax;
                audioSource.PlayOneShot(dashOnFx, 1f);
                audioSource.PlayOneShot(dashOn2Fx, 1f);
                anim.SetBool("isKO", false);

                GameObject[] enmProj = GameObject.FindGameObjectsWithTag("EnemyProj");
                if(enmProj != null)
                {
                    for (int i = 0; i < enmProj.Length; i++)
                    {
                        Destroy(enmProj[i].gameObject);
                    }
                }

                isDown = false;
            }
        }
    }

    void PlayerDash()
    {
        // Wait for dash mode input 
        if (Input.GetButtonDown("Fire2") && !isDown)
        {
            // Toggle dash mode
            if (!canDash)// && dashAmt - 1 >= 0)
            {
                DashFreeze();
                // SFX
                audioSource.PlayOneShot(dashOnFx, 1f);
                audioSource.PlayOneShot(dashOn2Fx, 1f);
            } else {
                DashUnfreeze();
                // Set animation
                anim.SetBool("dashAct", false);
            }
        }

        // If dash mode is true
        if (canDash)
        {
            // Determine mouse position
            Vector3 dashPos = Input.mousePosition + new Vector3(0, 0, 10);
            dashPos = Camera.main.ScreenToWorldPoint(dashPos);

            // Draw line
            lr.SetPosition(0, transform.position);
            lr.SetPosition(1, dashPos);

            // When dashing
            if (Input.GetButtonDown("Fire1"))
            {
                // Set animation
                anim.SetBool("dashAct", true);

                // SFX
                audioSource.PlayOneShot(swordFx, 1f);

                // Raycast piercing, return array
                RaycastHit2D[] hit = Physics2D.RaycastAll(transform.position, dashPos - transform.position, Vector2.Distance(transform.position, dashPos));
                
                Debug.DrawRay(transform.position, dashPos - transform.position, Color.red, 2f);

                // For all in hit array, affect
                for (int i = 0; i < hit.Length; i++)
                {
                    if(hit[i].transform.gameObject.tag == "Enemy")
                    {
                        Enemy enm = hit[i].transform.gameObject.GetComponent<Enemy>();
                        enm.health -= dashDmg;
                        enm.BossDmg();

                        // Destroy enemy if 0 health
                        if (enm.health <= 0)
                            enm.DestroySelf();

                        // SFX
                        audioSource.PlayOneShot(dashOn2Fx, .75f);
                    }
                }

                // Movement
                transform.position = dashPos;

				// Reduce dash count
				//dashAmt -= 1;
                UpdateDashUI();

                // Stop dash mode if out of dashes
                if (dashAmt <= 0 || dashAmt - 1 < 0)
				{
                    DashUnfreeze();

                    // Set animation
                    anim.SetBool("dashAct", false);
                }
            }

        }
    }

    void DashFreeze()
    {
        canDash = true;
        lr.enabled = true;
        anim.SetBool("animCanDash", true);
        cam.rtSmoothSpeed = cam.smoothDashSpeed;
        ppProf.GetSetting<ColorGrading>().saturation.Override(-100f);
        Time.timeScale = 0.05f;
    }

    void DashUnfreeze()
    {
        canDash = false;
        lr.enabled = false;
        anim.SetBool("animCanDash", false);
        cam.rtSmoothSpeed = cam.smoothSpeed;
        ppProf.GetSetting<ColorGrading>().saturation.Override(0f);
        Time.timeScale = 1f;
    }


}
