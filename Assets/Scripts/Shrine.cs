﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Shrine : MonoBehaviour
{
    public int health;
    public int healthMax;
    public Image healthRemainBar;

    ParticleSystem ps;

    public GameObject GOPanel;
    bool GOcheck = false;
    public GameObject tutorialPanel;

    void Start()
    {
        health = healthMax;
        ps = GetComponent<ParticleSystem>();
    }

    public void TakeDmg()
    {
        health -= 1;
        healthRemainBar.fillAmount = (float)health / (float)healthMax;
        if (health <= 0 && GOcheck == false)
        {
            ps.Play();
            StartCoroutine("GameOver");
            GOcheck = true;
        }
    }

    IEnumerator GameOver()
    {
        Time.timeScale = 0.045f;
        Debug.Log(Time.timeScale);
        yield return new WaitForSeconds(.06f);
        Time.timeScale = 0f;
        Debug.Log(Time.timeScale);
        GOPanel.SetActive(true);
        yield return null;
    }

    public void StartGame()
    {
        if (!tutorialPanel.gameObject.active)
            SceneManager.LoadScene("SampleScene", LoadSceneMode.Single);
    }

    public void RestartGame()
    {
        if (!tutorialPanel.gameObject.active)
        {
            Time.timeScale = 1f;
            Application.LoadLevel(Application.loadedLevel);
        }
    }

    public void TutorialShow()
    {
        if (!tutorialPanel.gameObject.active)
            tutorialPanel.gameObject.SetActive(true);
        else
            tutorialPanel.gameObject.SetActive(false);
    }
}
