﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathSound : MonoBehaviour
{
    public AudioClip shatterFx;
    public AudioClip impactFx;

    void Start()
    {
        // SFX
        GetComponent<AudioSource>().PlayOneShot(shatterFx, .75f);
        //GetComponent<AudioSource>().PlayOneShot(impactFx, .75f);
        Invoke("DestroySelf", 1f);
    }

    public void DestroySelf()
    {
        Destroy(gameObject);
    }
}
