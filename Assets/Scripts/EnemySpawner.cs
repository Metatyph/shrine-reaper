﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemySpawner : MonoBehaviour
{
    [Header("Prefabs")]
    public GameObject enemyPrefab;
    public GameObject bossPrefab;

    public float radius;
    Transform spawnPos;

    public int waveCount;

    public int waveAmt;
    public int waveSpawned;
    public int waveRemain;

    public int bossFactor;

    public GameObject[] wavePos;
    public Image[] waveImg;
    public Sprite[] counterSprite;

    [Header("UI")]
    public Image waveRemainBar;

    void Start()
    {
        waveRemain = waveAmt;
        waveCount = 1;

        InvokeRepeating("SpawnEnemy", 1f, 1f);

        UpdateUI();
        InitWaveCount();
        UpdateWaveCount();
    }

    void SpawnEnemy()
    {
        if (waveSpawned < waveAmt)
        {
            Vector2 randomPos = Random.insideUnitCircle.normalized;
            Vector3 spawnPos = new Vector3(randomPos.x, randomPos.y, 0) * radius;
            Instantiate(enemyPrefab, spawnPos, transform.rotation);
            waveSpawned += 1;

            UpdateUI();
        }
    }

    public void EnemyDown()
    {
        // Reduce remaining enemy count by 1
        waveRemain -= 1;

        // Check if all enemies in wave are depleted
        if(waveRemain == 0)
        {
            // Wave count up
            waveCount += 1;

            // Check if a boss wave is coming up
            if (waveCount % bossFactor == 0)
            {
                // If yes, initiate boss wave
                // Instantiate boss object
                Instantiate(bossPrefab, new Vector3(0, 6, 0), transform.rotation);

                // Increase amount of spawns
                waveAmt += 2;

                // Reset values, accounting for boss spawn
                waveSpawned = 0;
                waveRemain = waveAmt + 1;
            } else {
                // Otherwise, initiate next normal wave
                // Increase amount of spawns
                waveAmt += 2;

                // Reset values
                waveSpawned = 0;
                waveRemain = waveAmt;
            }

            UpdateWaveCount();
        }

        UpdateUI();

    }

    void UpdateUI()
    {
        if (waveCount % bossFactor != 0)
            waveRemainBar.fillAmount = (float)waveRemain / (float)waveAmt;
        else
            waveRemainBar.fillAmount = (float)waveRemain / (float)(waveAmt + 1);
    }

    void InitWaveCount()
    {
        for (int i = 0; i < wavePos.Length; i++)
        {
            waveImg[i] = wavePos[i].GetComponent<Image>();
        }
    }

    void UpdateWaveCount()
    {
        int fullCountAmt = Mathf.FloorToInt((float)waveCount / 5f);

        // If more than one full wave counter
        if (waveCount > 5)
        {
            for (int i = 0; i < fullCountAmt; i++)
            {
                waveImg[i].color = Color.white + new Color(0, 0, 0, 1);
                waveImg[i].sprite = counterSprite[4];
            }

            waveImg[fullCountAmt].color = Color.white + new Color(0, 0, 0, 1);
            waveImg[fullCountAmt].sprite = counterSprite[(waveCount % 5)];
        } else
        {
            waveImg[0].color = Color.white + new Color(0, 0, 0, 1);
            waveImg[0].sprite = counterSprite[waveCount - 1];
        }

    }

}
