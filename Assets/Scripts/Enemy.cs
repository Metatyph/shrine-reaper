﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    public float health;
    float healthMax;
    public GameObject projectile;

    public float speed;
    float rtSpeed;
    public float approachRadius;

    Vector3 target;

    EnemySpawner spawner;
    Shrine shrine;

    public GameObject deathPrefab;

    public string[] atkQueue;

    public GameObject bossHealthFill;
    public GameObject bossHealthInd;
    public GameObject Rotate_L;
    public GameObject Rotate_R;

    public Sprite hurt;
    public bool isHurt;
    public Animator anim;

    void Start()
    {
        rtSpeed = speed;
        target = GameObject.Find("shrinebox").transform.position;
        anim = GetComponent<Animator>();

        healthMax = health;

        spawner = GameObject.Find("EnemySpawner").GetComponent<EnemySpawner>();
        shrine = GameObject.Find("shrinebox").GetComponent<Shrine>();

        for (int i = 0; i < atkQueue.Length; i++)
        {
            StartCoroutine(atkQueue[i]);
        }

        if (bossHealthFill != null)
        {
            InvokeRepeating("BossTax", 2f, 2f);
        }
    }

    void BossTax()
    {
        shrine.TakeDmg();
    }

    void Update()
    {
        // Move towards shrine, up to specified radius
        if (Vector2.Distance(transform.position, target) > approachRadius)
            transform.position = Vector3.MoveTowards(transform.position, target, rtSpeed * Time.deltaTime);

        // If near shrine, do damage
        if (Vector2.Distance(transform.position, target) <= approachRadius)
            shrine.TakeDmg();
    }

    public void BossDmg()
    {
        if(bossHealthFill != null)
        {
            bossHealthFill.transform.localScale = new Vector3(health / healthMax, 1, 1);
            StartCoroutine("DmgIndicator");
        }
    }

    public void DmgFeedback()
    {
        StartCoroutine("DmgFeedbackCR");
    }

    IEnumerator DmgFeedbackCR()
    {
        anim.SetBool("isHurt", true);
        yield return new WaitForSeconds(.5f);
        anim.SetBool("isHurt", false);
        yield return null;
    }

    IEnumerator DmgIndicator()
    {
        float t = 0f;
        yield return new WaitForSeconds(1f);
        while(bossHealthFill.transform.localScale.x < bossHealthInd.transform.localScale.x)
        {
            bossHealthInd.transform.localScale = new Vector3(Mathf.Lerp(bossHealthInd.transform.localScale.x, bossHealthFill.transform.localScale.x, t), 1, 1);
            t += 0.1f * Time.deltaTime;
            yield return new WaitForSeconds(.25f);
        }
        yield return null;
    }

    public void DestroySelf()
    {
        GetComponent<BoxCollider2D>().enabled = false;
        Instantiate(deathPrefab, transform.position, transform.rotation);
        spawner.EnemyDown();
        StartCoroutine("DeathFeedback");
    }

    IEnumerator DeathFeedback()
    {
        // Set entire queue to null
        for (int i = 0; i < atkQueue.Length; i++)
        {
            atkQueue[i] = "Atk_Null";
        }
        // Execute new nullified queue
        for (int i = 0; i < atkQueue.Length; i++)
        {
            StartCoroutine(atkQueue[i]);
        }

        // Insert feedback here
        rtSpeed = 0;
        anim.SetBool("isHurt", true);

        yield return new WaitForSeconds(.5f);

        Destroy(gameObject);

        yield return null;
    }

    // Attack routines
    IEnumerator Atk_Null()
    {
        yield return null;
    }
    
    IEnumerator AtkRoutine_Basic()
    {
        while (true)
        {
            yield return new WaitForSeconds(4f);
            rtSpeed = 0;
            yield return new WaitForSeconds(1.5f);
            Spread(8);
            yield return new WaitForSeconds(.1f);
            rtSpeed = speed;

            yield return null;
        }
    }

    IEnumerator BossRoutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(.75f);
            Spread_Child("l", 16);
            yield return new WaitForSeconds(.75f);
            Spread_Child("r", 16);
            yield return new WaitForSeconds(.75f);
            Spread_Child("l", 16);
            Spread_Child("r", 16);
            yield return null;
        }
    }

    // Attack patterns
    void Spread(int amt)
    {
        for (int i = 0; i < amt; i++)
        {
            GameObject projClone = Instantiate(projectile, transform.position, Quaternion.Euler(Vector3.zero + new Vector3(0, 0, (360 / amt) * i)));
        }
    }

    void Spread_Child(string dir, int amt)
    {
        for (int i = 0; i < amt; i++)
        {
            GameObject projClone = Instantiate(projectile, transform.position, Quaternion.Euler(Vector3.zero + new Vector3(0, 0, (360 / amt) * i)));

            if (dir == "l")
                projClone.transform.parent = Rotate_L.transform;
            else if (dir == "r")
                projClone.transform.parent = Rotate_R.transform;
        }
    }
}
