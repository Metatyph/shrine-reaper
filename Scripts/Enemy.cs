﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float health;
    public GameObject projectile;

    public float speed;
    float rtSpeed;
    public float approachRadius;

    Vector3 target;

    EnemySpawner spawner;

    void Start()
    {
        rtSpeed = speed;
        target = GameObject.Find("shrinebox").transform.position;

        spawner = GameObject.Find("EnemySpawner").GetComponent<EnemySpawner>();

        StartCoroutine("AtkRoutine_Basic");
    }

    void Update()
    {
        if (Vector2.Distance(transform.position, target) > approachRadius)
            transform.position = Vector3.MoveTowards(transform.position, target, rtSpeed * Time.deltaTime);
    }

    public void DestroySelf()
    {
        spawner.EnemyDown();
        Destroy(gameObject);
    }

    // Attack routines
    IEnumerator AtkRoutine_Basic()
    {
        while (true)
        {
            yield return new WaitForSeconds(4f);
            rtSpeed = 0;
            yield return new WaitForSeconds(1.5f);
            AtkPattern_Spread();
            yield return new WaitForSeconds(.1f);
            rtSpeed = speed;

            yield return null;
        }
    }

    // Attack patterns
    void AtkPattern_Spread()
    {
        Instantiate(projectile, transform.position, Quaternion.Euler(Vector3.zero));
        Instantiate(projectile, transform.position, Quaternion.Euler(0, 0, 45f));
        Instantiate(projectile, transform.position, Quaternion.Euler(0, 0, 90f));
        Instantiate(projectile, transform.position, Quaternion.Euler(0, 0, 135f));
        Instantiate(projectile, transform.position, Quaternion.Euler(0, 0, 180f));
        Instantiate(projectile, transform.position, Quaternion.Euler(0, 0, -45f));
        Instantiate(projectile, transform.position, Quaternion.Euler(0, 0, -90f));
        Instantiate(projectile, transform.position, Quaternion.Euler(0, 0, -135f));
    }
}
