﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spirits : MonoBehaviour
{
	Vector3 target;
	float speed;
	float speedInc;

	void Start()
	{
		speed = 1;
		speedInc = 0;
		target = GameObject.Find("shrinebox").transform.position;

		InvokeRepeating("SpeedUp", .1f, .1f);
	}

	void SpeedUp()
	{
		speed += 1 + speedInc;
		speedInc += 2;
	}

	void Update()
	{
		transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);
	}
}
