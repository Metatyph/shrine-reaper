﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject enemyPrefab;
    public float radius;
    Transform spawnPos;

    public int waveCount;

    public int waveAmt;
    public int waveSpawned;
    public int waveRemain;

    void Start()
    {
        waveRemain = waveAmt;
        waveCount = 1;

        InvokeRepeating("SpawnEnemy", 1f, 1f);
    }

    void SpawnEnemy()
    {
        if (waveSpawned < waveAmt)
        {
            Vector2 randomPos = Random.insideUnitCircle.normalized;
            Vector3 spawnPos = new Vector3(randomPos.x, randomPos.y, 0) * radius;
            Instantiate(enemyPrefab, spawnPos, transform.rotation);
            waveSpawned += 1;
        }
    }

    public void EnemyDown()
    {
        waveRemain -= 1;
        if(waveRemain == 0)
        {
            // Wave count up
            waveCount += 1;

            // Increase amount of spawns
            waveAmt += 10;

            // Reset values
            waveSpawned = 0;
            waveRemain = waveAmt;
        }
    }
}
