﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
	[Header("UI Elements")]
    public GameObject dashBarFill;

    [Header("Movement")]
    public float moveSpd;
    Rigidbody2D rb2d;
    float rtMoveSpd;
    float movHor;
    float movVert;

    [Header("Targetting & Projectiles")]
    public GameObject targetting;
    public GameObject bulletPrefab;

    [Header("Bullets")]
    public float bulletSpeed;
    public float bulletLife;
    public float bulletDmg;
    float rtBulletSpeed;
    float rtBulletLife;
    float rtBulletDmg;

    [Header("Dashing")]
    public float dashDmg;
    public float dashMax;
    public float dashAmt;
    public float dashInc;
    float rtDashDmg;
    LineRenderer lr;
    bool canDash = false;

    [Header("SFX")]
    public AudioClip swordFx;
    AudioSource audioSource;

    [Header("Post Processing")]
    public PostProcessProfile ppProf;
    CameraController cam;

	void Start()
    {
        // Initialize RigidBody
        rb2d = gameObject.GetComponent<Rigidbody2D>();

        // Initialize all runtime values
        rtMoveSpd = moveSpd;
        rtBulletSpeed = bulletSpeed;
        rtBulletLife = bulletLife;
        rtBulletDmg = bulletDmg;
        rtDashDmg = dashDmg;

        // Initialize audio
        audioSource = GetComponent<AudioSource>();

		// Set dispensable values to max
		dashAmt = dashMax;

        // Initialize UI
        cam = Camera.main.GetComponent<CameraController>();
        UpdateDashUI();

		// Initialize post processing stack
		ppProf.GetSetting<ColorGrading>().saturation.Override(0f);

	}

	void Update()
	{
		PlayerMovement();
		PlayerRotation();
		PlayerAttack();
        PlayerDash();
    }

    public void UpdateDashUI()
    {
        dashBarFill.transform.localScale = new Vector3(dashAmt / dashMax, 1, 1);
    }

    void PlayerRotation()
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos = Camera.main.ScreenToWorldPoint(mousePos);

        Vector2 direction = new Vector2(mousePos.x - targetting.transform.position.x, mousePos.y - targetting.transform.position.y);
        targetting.transform.up = direction;
    }

    void PlayerMovement()
    {
        if (!canDash)
        {
            // Horizontal movement
            if (Input.GetButton("Horizontal"))
            {
                movHor = rtMoveSpd * Input.GetAxisRaw("Horizontal");
            }
            else
            {
                movHor = 0;
            }

            // Vertical movement
            if (Input.GetButton("Vertical"))
            {
                movVert = rtMoveSpd * Input.GetAxisRaw("Vertical");
            }
            else
            {
                movVert = 0;
            }

            // If moving diagonally, reduce movement speed
            if (Input.GetButton("Horizontal") && Input.GetButton("Vertical"))
            {
                rtMoveSpd = moveSpd * .65f;
            }

			// Move player
			transform.Translate(movHor, movVert, 0);
        }
    }

    // Basic bullet attack
    void PlayerAttack()
    {
        if (Input.GetButtonDown("Fire1") && !canDash)
        {
            // Instantiate bullets as bulletClone
            GameObject bulletClone;
            bulletClone = Instantiate(bulletPrefab, transform.position, targetting.transform.rotation);

            // Get bullet script from instantiated bullets
            Bullet bul = bulletClone.gameObject.GetComponent<Bullet>();

            // Modify bullet variables
            bul.speed = rtBulletSpeed;
            bul.lifeTime = rtBulletLife;
            bul.damage = rtBulletDmg;

            // Camera shake
            if ((targetting.transform.rotation.z < Quaternion.Euler(0, 0, -45).z && targetting.transform.rotation.z > Quaternion.Euler(0, 0, -135).z) || (targetting.transform.rotation.z > Quaternion.Euler(0, 0, 45).z && targetting.transform.rotation.z < Quaternion.Euler(0, 0, 135).z)) 
                cam.CameraShake("x", -.1f * Mathf.Sign(targetting.transform.rotation.z));
            else
            {
                if(targetting.transform.rotation.z < Quaternion.Euler(0, 0, 45).z && targetting.transform.rotation.z > Quaternion.Euler(0, 0, -45).z)
                    cam.CameraShake("y", .1f);
                else
                    cam.CameraShake("y", -.1f);
            }
        }
    }

    void PlayerDash()
    {
        // Wait for dash mode input 
        if (Input.GetButtonDown("Fire3"))
        {
            // Get LineRenderer component
            lr = GetComponent<LineRenderer>();

            // Toggle dash mode
            if (!canDash && dashAmt - 1 >= 0)
            {
                DashFreeze();
			} else {
                DashUnfreeze();
            }
        }

        // If dash mode is true
        if (canDash)
        {
            // Determine mouse position
            Vector3 dashPos = Input.mousePosition + new Vector3(0, 0, 10);
            dashPos = Camera.main.ScreenToWorldPoint(dashPos);

            // Draw line
            lr.SetPosition(0, transform.position);
            lr.SetPosition(1, dashPos);

            // When dashing
            if (Input.GetButtonDown("Fire1"))
            {
                // SFX
                //audioSource.PlayOneShot(swordFx, 1f);

                // Raycast piercing, return array
                RaycastHit2D[] hit = Physics2D.RaycastAll(transform.position, dashPos - transform.position, Vector2.Distance(transform.position, dashPos));
                
                Debug.DrawRay(transform.position, dashPos - transform.position, Color.red, 2f);

                // For all in hit array, affect
                for (int i = 0; i < hit.Length; i++)
                {
                    if(hit[i].transform.gameObject.tag == "Enemy")
                    {
                        Enemy enm = hit[i].transform.gameObject.GetComponent<Enemy>();
                        enm.health -= rtDashDmg;

                        // Destroy enemy if 0 health
                        if (enm.health <= 0)
                            enm.DestroySelf();
                    }
                }

                // Movement
                transform.position = dashPos;

				// Reduce dash count
				dashAmt -= 1;
                UpdateDashUI();

                // Stop dash mode if out of dashes
                if (dashAmt <= 0 || dashAmt - 1 < 0)
				{
                    DashUnfreeze();
                }
            }

        }
    }

    void DashFreeze()
    {
        canDash = true;
        lr.enabled = true;
        cam.rtSmoothSpeed = cam.smoothDashSpeed;
        ppProf.GetSetting<ColorGrading>().saturation.Override(-100f);
        Time.timeScale = 0.05f;
    }

    void DashUnfreeze()
    {
        canDash = false;
        lr.enabled = false;
        cam.rtSmoothSpeed = cam.smoothSpeed;
        ppProf.GetSetting<ColorGrading>().saturation.Override(0f);
        Time.timeScale = 1f;
    }


}
