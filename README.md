# Shrine Reaper

Hello! This is the source code for [Shrine Reaper](https://icun-p.itch.io/shrine-reaper), a 72-hour Game Jam project for [Mini Jam 25: Spirits](https://itch.io/jam/mini-jam-25-spirits).

The sound effects were all used under a license- therefore, they are not included. 